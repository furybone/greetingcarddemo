package com.cards.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cards.models.GreetingCard;
import com.cards.models.dao.CardDao;

import springfox.documentation.annotations.ApiIgnore;

@Controller
@RequestMapping(value="")
@ApiIgnore
public class CardsViewController {

	@Autowired
	CardDao cardDao;
	
	@GetMapping(value="/")
	public String welcomePage(){
		
		return "welcome";
	}
	
	@GetMapping(value="/greetingCard/{cardId}")
	public String view(Map<String,Object> model, @PathVariable int cardId){
		
		GreetingCard card = cardDao.findOne(cardId);
		if(card == null)
			return "404";
		
		model.put("cssName", card.getTemplate().getCssName().toString());
		model.put("body", card.getBody());
		model.put("subject", card.getSubject());
		
		return "card";
	}
}
