package com.cards.controllers;

import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cards.models.Template;
import com.cards.models.dao.CardDao;
import com.cards.models.requests.CardRequest;
import com.cards.models.requests.CardTemplateRequest;
import com.cards.services.CardsService;

@RestController
@RequestMapping(value="greetingCard")
public class CardsController {

	@Autowired
	CardsService cardsService;
	
	@GetMapping(value="Catalog")
	public ResponseEntity<ArrayList<Template>> getCatalog(){
	
		return cardsService.getCatalog();
	}
	
	@PostMapping(value="/{templateId}")
	public ResponseEntity createCard(@Valid @PathVariable int templateId,
										@RequestBody CardRequest card){
		
		return cardsService.createCard(templateId, card);
	}
	
	@PutMapping()
	public ResponseEntity createCardTemplate(@RequestBody CardTemplateRequest cardTemplateRequest){
	
		return cardsService.createCardTemplate(cardTemplateRequest);
	}
	
	@GetMapping(value="/")
	public ResponseEntity getAllCards(){
		
		return cardsService.getAllCards();
	}
}