package com.cards.services;

import java.util.ArrayList;

import javax.management.RuntimeErrorException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.cards.models.GreetingCard;
import com.cards.models.Template;
import com.cards.models.dao.CardDao;
import com.cards.models.dao.TemplateDao;
import com.cards.models.requests.CardRequest;
import com.cards.models.requests.CardTemplateRequest;

@Service
public class CardsService {

	@Autowired
	TemplateDao templateDao;
	
	@Autowired
	CardDao cardDao;
	
	public ResponseEntity<ArrayList<Template>> getCatalog(){
		
		ArrayList<Template> catalog = (ArrayList<Template>) templateDao.findAll();
		
		return new ResponseEntity<ArrayList<Template>>(catalog,HttpStatus.OK);
	}
		
	public ResponseEntity<GreetingCard> createCard(int templateId, CardRequest cardRequest){
		Template template = templateDao.findOne(templateId);
		if(template == null)
			throw new RuntimeException("Template not found.");
		
		GreetingCard card = new GreetingCard(template);
		card.replaceAllSubjectArguments(cardRequest);
		card = cardDao.save(card);
		
		return new ResponseEntity<GreetingCard>(card, HttpStatus.CREATED);
	}
	
	public ResponseEntity<Template> createCardTemplate(CardTemplateRequest cardTemplateRequest){
		
		Template template = new Template();
		template.setSubject(cardTemplateRequest.getSubject());
		template.setCssName(cardTemplateRequest.getCssName());
		template.setBody(cardTemplateRequest.getBody());
		template = templateDao.save(template);
		template.setTemplateArguments(cardTemplateRequest.getArguments());
		template = templateDao.save(template);
		return new ResponseEntity<Template>(template, HttpStatus.CREATED);
	}	
	
	public ResponseEntity<ArrayList<GreetingCard>> getAllCards(){
		
		ArrayList<GreetingCard> cards = (ArrayList<GreetingCard>) cardDao.findAll();
		return new ResponseEntity<ArrayList<GreetingCard>>(cards, HttpStatus.OK);
	}
}
