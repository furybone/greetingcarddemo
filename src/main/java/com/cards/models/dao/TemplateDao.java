package com.cards.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cards.models.Template;

public interface TemplateDao extends JpaRepository<Template, Integer>{

	Template findOneBySubject(String subject);
}
