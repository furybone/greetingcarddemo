package com.cards.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cards.models.GreetingCard;

public interface CardDao extends JpaRepository<GreetingCard, Integer> {

}
