package com.cards.models;

import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import com.cards.models.requests.ArgumentsRequest;
import com.cards.models.requests.CardRequest;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author roys
 *
 */

@Entity
@Table
public class GreetingCard {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "template_id", nullable = false)
	@JsonIgnore
	private Template template;

	private String subject;
	
	private String body;
	
	private Date createDate;

	private Date updateDate;
	
	public GreetingCard(){}
	
	public GreetingCard(Template template){
		
		this.setTemplate(template);
	}

	/**
	 * iterate and serach for keys and set their value in the greeting card record.
	 * @param cardRequest
	 */
	public void replaceAllSubjectArguments(CardRequest cardRequest){
		
		Set<ArgumentsRequest> arguments = cardRequest.getArguments();
		this.setBody(this.getTemplate().getBody());
		this.setSubject(this.getTemplate().getSubject());
		
		for (ArgumentsRequest argumentRequest : arguments) {
			
			String key = argumentRequest.getKey();
			String value = argumentRequest.getValue();
			
			this.setBody(this.getBody().replaceAll("\\{"+key+"\\}", value));
			this.setSubject(this.getSubject().replaceAll("\\{"+key+"\\}", value));
		}
	
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Template getTemplate() {
		return template;
	}

	public void setTemplate(Template template) {
		this.template = template;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@PrePersist
	protected void onCreate() {
		createDate = new Date();
		updateDate = new Date();
	}

	@PreUpdate
	protected void onUpdate() {
		updateDate = new Date();
	}
}
