package com.cards.models;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author roys
 *
 */
@Entity
@Table
public class Template {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Enumerated(EnumType.STRING)
	private EnumCss cssName = EnumCss.white; //default
	
	private String subject;
	
	private String body;

	@OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="template")
	private List<TemplateArgument> templateArguments;
	
	public Template(){}
	
	public Template(int id) {
		this.setId(id);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EnumCss getCssName() {
		return cssName;
	}

	public void setCssName(EnumCss cssName) {
		this.cssName = cssName;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public List<TemplateArgument> getTemplateArguments() {
		return templateArguments;
	}

	public void setTemplateArguments(List<TemplateArgument> templateArguments) {
		for (TemplateArgument templateArgument : templateArguments) {
			templateArgument.setTemplate(new Template(this.getId()));
		}
		this.templateArguments = templateArguments;
	}
	
}
