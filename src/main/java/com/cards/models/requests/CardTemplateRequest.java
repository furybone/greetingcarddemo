package com.cards.models.requests;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.cards.models.EnumCss;
import com.cards.models.TemplateArgument;

public class CardTemplateRequest {


	@Enumerated(EnumType.STRING)
	private EnumCss cssName = EnumCss.white;
	
	private String subject;
	
	private String body;
	
	private List<TemplateArgument> arguments = new ArrayList<>();

	public List<TemplateArgument> getArguments() {
		return arguments;
	}

	public void setArguments(List<TemplateArgument> arguments) {
		this.arguments = arguments;
	}

	public EnumCss getCssName() {
		return cssName;
	}

	public void setCssName(EnumCss cssName) {
		this.cssName = cssName;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
		
}
