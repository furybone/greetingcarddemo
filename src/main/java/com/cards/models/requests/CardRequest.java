package com.cards.models.requests;

import java.util.HashSet;
import java.util.Set;

/**
 * @author roys
 *
 */
public class CardRequest {
	
	private Set<ArgumentsRequest> arguments = new HashSet<>();

	public Set<ArgumentsRequest> getArguments() {
		return arguments;
	}

	public void setArguments(Set<ArgumentsRequest> arguments) {
		this.arguments = arguments;
	}
		
}
