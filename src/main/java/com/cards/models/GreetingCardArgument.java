package com.cards.models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class GreetingCardArgument {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String name;
	
	private String value;

	public int getId() {
		return id;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "greeting_card_id", nullable = false)
	private GreetingCard greetingCard = new GreetingCard();

	public GreetingCardArgument(){}
	
	public GreetingCardArgument(String key, String value,GreetingCard greetingCard){
	
		this.setName(key);
		this.setValue(value);
		this.setGreetingCard(greetingCard);
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public GreetingCard getGreetingCard() {
		return greetingCard;
	}

	public void setGreetingCard(GreetingCard greetingCard) {
		this.greetingCard = greetingCard;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
