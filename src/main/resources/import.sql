INSERT INTO Template (id, css_name, subject, body) VALUES (1, 'white', 'Happy Birthday!', 'Dear {name}, I wish you.. </br> Yours, {userName}');
INSERT INTO Template (id, css_name, subject, body) VALUES (2, 'red', 'Happy Valentines day', 'Dear {name}, I wish you.. </br> Yours, {userName}');
INSERT INTO Template (id, css_name, subject, body) VALUES (3, 'pink', 'Congratulations on your newborn', 'Dear {name}, I wish you the best for your new {boyOrGirl} ... </br> Yours, {userName}');
INSERT INTO Template (id, css_name, subject, body) VALUES (4, 'blue', 'Happy Holiday', 'Dear {name}, I wish you... </br> Yours, {userName}');
  
INSERT INTO Template_Argument (name, template_id) VALUES ('name', 1);
INSERT INTO Template_Argument (name, template_id) VALUES ('userName', 1);
INSERT INTO Template_Argument (name, template_id) VALUES ('name', 2);
INSERT INTO Template_Argument (name, template_id) VALUES ('userName', 2);
INSERT INTO Template_Argument (name, template_id) VALUES ('name', 3);
INSERT INTO Template_Argument (name, template_id) VALUES ('boyOrGirl', 3);
INSERT INTO Template_Argument (name, template_id) VALUES ('userName', 3);
INSERT INTO Template_Argument (name, template_id) VALUES ('name', 4);
INSERT INTO Template_Argument (name, template_id) VALUES ('userName', 4);